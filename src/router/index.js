import { createRouter, createWebHistory } from 'vue-router';

import PageNotFound from '../components/PageNotFound'
import Friends from '../components/Friends'
import Profile from '../components/Profile'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            redirect: () => {
                return {
                    path: 'friends'
                }
            }
        },
        {
            name: 'Friends',
            path: '/friends',
            component: Friends
        },
        {
            name: 'Profile',
            path: '/friends/profile/:id',
            component: Profile,
        },
        {
            name: 'PageNotFound',
            path: '/:pathMatch(.*)*',
            component: PageNotFound
        }
    ]
});

export default router;